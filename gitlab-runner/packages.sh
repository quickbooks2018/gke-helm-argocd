#!/bin/bash

 apt update -y && apt install -y unzip sudo nano vim && curl https://get.datree.io | /bin/bash
 curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 && chmod 700 get_helm.sh && ./get_helm.sh && rm -f get_helm
 
 # docker-slim installation
 curl -# -LO https://downloads.dockerslim.com/releases/1.39.0/dist_linux.tar.gz && tar -xzvf dist_linux.tar.gz && cd dist_linux && mv * /usr/local/bin/ && cd .. && rm -rf dist_linux.tar.gz dist_linux
 curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    unzip awscliv2.zip \
    sudo ./aws/install \
    rm -rf *.zip \
    aws --version
 curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
    && chmod +x kubectl \
    && mv kubectl /usr/local/bin/kubectl
 curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 \
    && chmod 700 get_helm.sh \
    && ./get_helm.sh \
    && rm -f get_helm.sh

 # End   
