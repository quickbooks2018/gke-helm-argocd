# helm GKE Argocd

##### Gitlab Secure Files for CICD
- https://docs.gitlab.com/ee/ci/secure_files/
-  SECURE_FILES_DOWNLOAD_PATH: './where/files/should/go/'
```gitlab
- curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash
```

```gke
- https://cloud.google.com/kms/docs/encrypt-decrypt
# GCP KMS Encryption & Decryption

gcloud kms encrypt \
    --key key \
    --keyring key-ring \
    --location location  \
    --plaintext-file file-with-data-to-encrypt \
    --ciphertext-file file-to-store-encrypted-data


gcloud kms decrypt \
    --key key \
    --keyring key-ring \
    --location location  \
    --ciphertext-file file-path-with-encrypted-data \
    --plaintext-file file-path-to-store-plaintext

# PATH Copied: projects/playground-s-11-027aa59c/locations/us-central1/keyRings/cloudgeeks-key-ring/cryptoKeys/cloudgeeks
############
# Encryption
############
gcloud kms encrypt \
    --key projects/playground-s-11-027aa59c/locations/us-central1/keyRings/cloudgeeks-key-ring/cryptoKeys/cloudgeeks \
    --keyring projects/playground-s-11-027aa59c/locations/us-central1/keyRings/cloudgeeks-key-ring \
    --location us-central1  \
    --plaintext-file service_account.txt \
    --ciphertext-file service_account.json-ENCCRYPTED   

############
# Decryption 
############
gcloud kms decrypt \
    --key projects/playground-s-11-027aa59c/locations/us-central1/keyRings/cloudgeeks-key-ring/cryptoKeys/cloudgeeks \
    --keyring projects/playground-s-11-027aa59c/locations/us-central1/keyRings/cloudgeeks-key-ring \
    --location us-central1  \
    --ciphertext-file service_account.json-ENCCRYPTED \
    --plaintext-file service_account.json

############################
# Login with Service account
############################
# gcloud auth activate-service-account SERVICE_ACCOUNT@DOMAIN.COM --key-file=/path/key.json --project=PROJECT_ID

export service_account=' cli-service-account-1@playground-s-11-027aa59c.iam.gserviceaccount.com'
export key_json='service_account.json'
export project_id='playground-s-11-027aa59c'
gcloud auth activate-service-account ${service_account} --key-file=${key_json} --project=${project_id}

################################
# Google Artifact Registry Login
#################################
# Note: create a container repo 

# To avoid inputs

yes | gcloud auth configure-docker us-central1-docker.pkg.dev

OR 

gcloud auth configure-docker us-central1-docker.pkg.dev --quiet

#####################
GCP Artifact Repo TAG
#####################
export IMAGE_NAME=quickbooks2018/datree
export IMAGE_TAG=75
export project_id='playground-s-11-027aa59c'
export artifact_registry_name='cloudgeeks'
export gcr_artifact='us-central1-docker.pkg.dev'
export gcr_artifact_registry="${gcr_artifact}/${project_id}/${artifact_registry_name}/${IMAGE_NAME}"


docker tag ${IMAGE_NAME}:${IMAGE_TAG} ${gcr_artifact_registry}:${IMAGE_TAG}
docker push ${gcr_artifact_registry}:${IMAGE_TAG}


###########
# GCR Login
###########
gcloud auth configure-docker --quiet

###################
# GCR  Registry TAG 
###################
export IMAGE_NAME=quickbooks2018/datree
export IMAGE_TAG=75
export project_id='playground-s-11-027aa59c'
export registry_name='cloudgeeks'
export gcr='gcr.io'
export gcr_registry="${gcr}/${project_id}/${registry_name}"

docker tag ${IMAGE_NAME}:${IMAGE_TAG} ${gcr_registry}:${IMAGE_TAG}
docker push ${gcr_registry}:${IMAGE_TAG}
```

```helm
helm upgrade --install cloudgeeks cloudgeeks/ -f cloudgeeks/values.yaml --namespace cloudgeeks-app --create-namespace  \
--set image.tag=latest \
--set replicaCount=2
```

- Packing helm chart
```helm-package
helm package -d ${PWD}/helm ${PWD}/helm/cloudgeeks
```


# gitlab runners
- Docker Installation
- https://get.docker.com/
```
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```
- Gitlab Runner in Docker

- Important Learning Note: To make gitlab runner work with docker executor, gitlab runner should be running in Host machine not in container, so must use shell executor with gitlab docker.

- https://docs.gitlab.com/runner/install/docker.html

##### docker in docker
- https://docs.gitlab.com/runner/executors/docker.html
- https://gitlab.com/gitlab-org/gitlab-foss/-/issues/17769
- https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4794
```docker
docker exec -it gitlab-runner bash
cd /etc/gitlab-runner
apt update -y && apt install -y vim nano
vim config.toml
---> changed priviled ---> to true
---> changes volumes = ["/cache","/var/run/docker.sock:/var/run/docker.sock","/usr/bin/docker:/usr/bin/docker"]
docker restart gitlab-runner
```

- Linux

```
docker run -id --name gitlab-runner --restart unless-stopped --network host -w /mnt -v $(which docker):/usr/bin/docker -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
```

- Windows

- docker compose version 2

- https://docs.docker.com/compose/install/linux/#install-using-the-repository

```
docker run -id --name gitlab-runner --network host --restart unless-stopped -w /mnt -v "c:/users/Muhammad Asim/Desktop/kubernetes:/mnt" -v "//var/run/docker.sock:/var/run/docker.sock" gitlab/gitlab-runner:latest

docker exec -it gitlab-runner bash

curl -fsSL https://get.docker.com -o get-docker.sh

sh get-docker

rm -f get-docker

cd /home/gitlab-runner

cp -rv /mnt/.kube .
```



- https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner

- Ubuntu
```installation
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```

sudo apt-get install gitlab-runner

systemctl status gitlab-runner

systemctl enable gitlab-runner

- registration
```register
gitlab-runner register

gitlab-runner status

gitlab-runner list

gitlab-runner verify --delete -t uLxMwdxdC1vmbXtceomu -u https://gitlab.com/

gitlab-runner verify

gitlab-runner --help
```

- permissions
```docker
chmod 666 /var/run/docker.sock
```

- gitlab cicd setup

- .gitlab-ci.yml

```gitlab-ci.yml
image: python:latest

before_script:
# Install
  - apt update -y
  - apt install -y jq

build:
  script:
    - docker-compose --project-name app up -d --build
```
- .gitlab-ci.yml
```gitlab-ci.yaml
image: docker:19.03.1

variables:
    DOCKERFILE_LOCATION: ./
    DOCKER_VERSION: Docker-19.03.6-ce
    DOCKER_TLS_CERTDIR: ""

services:
  - docker:19.03.1-dind

stages:
  - build
  - deploy


###############
#   Builds    #
###############

build-docker-latest:
  stage: build
  environment:
    name: dev
  script:
    - echo "AWSCLI Installation"
    - docker ps -a
    - docker-compose --version

  only:
    - main
   

###############
#   Deploy    #
###############

deploy-to-application-repo:
  stage: deploy
  environment:
    name: dev
  script:
    - echo "hello"
  only:
    - main

```

- docker-compose installation

```
# https://docs.docker.com/compose/cli-command/
# https://docs.docker.com/compose/profiles/
# https://github.com/EugenMayer/docker-image-atlassian-jira/blob/master/docker-compose.yml

#########################################################################################
# 1 Run the following command to download the current stable release of Docker Compose
#########################################################################################

 mkdir -p ~/.docker/cli-plugins/
 curl -SL https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose
 
 ###############################################
 # 2 Apply executable permissions to the binary
 ###############################################
 
  chmod +x ~/.docker/cli-plugins/docker-compose
  
  ###############################################
  # 3 Apply executable permissions to the binary
  ###############################################
  
  docker compose version
  
  
  
  
  # Commands
  # Build a Specific Profile
 #  docker compose -p app up -d --build

###########################
# Docker Compose Version 1
###########################
# https://docs.docker.com/compose/install/

curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version
```

- docker-compose commands
```docker-compose

docker-compose up -d --build

docker-compose --project-name app up -d --build
```

- crontab entry for automated cleaning of dangling images
```dangling_images
0 8 * * * yes | docker image prune
```

- docker socket permissions
```
#!/bin/bash
# Purpose: Set Docker Socket Permissions after reboot & Docker Logging

###########################
# Docker Socket Permissions
###########################
cat <<EOF > ${HOME}/docker-socket.sh
#!/bin/bash
chmod 666 /var/run/docker.sock
#End
EOF

chmod +x ${HOME}/docker-socket.sh

cat <<EOF > /etc/systemd/system/docker-socket.service
[Unit]
Description=Docker Socket Permissions
After=docker.service
BindsTo=docker.service
ReloadPropagatedFrom=docker.service

[Service]
Type=oneshot
ExecStart=${HOME}/docker-socket.sh
ExecReload=${HOME}/docker-socket.sh
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOF

systemctl start docker-socket.service

systemctl enable docker-socket.service

################
# Docker Logging
################

cat << EOF > /etc/docker/daemon.json
{
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "1024m",
    "max-file": "5"
  }
}

EOF

shutdown -r now

# End
```

##### Datree Installation
- https://hub.datree.io/cli/getting-started
```datree
curl https://get.datree.io | /bin/bash
```
export DATREE_TOKEN=[YOU-TOKEN] # Grab that from webui

datree test datree.yaml -p prod

##### Helm Installation
- https://helm.sh/docs/intro/install/
```helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

##### ArgoCD Installation
- https://argo-cd.readthedocs.io/en/stable/getting_started/?_gl=1*ykz3xg*_ga*MTU4NDg2NjU0NS4xNjcxNzE5OTI0*_ga_5Z1VTPDL73*MTY3MTcxOTkyNS4xLjAuMTY3MTcxOTkyOC4wLjAuMA..
```argocd
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

kubectl port-forward svc/argocd-server -n argocd 9090:443
ssh -N -L 9090:0.0.0.0:9090 cloud_user@54.151.90.187

kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
```

##### ArgoCD Private Repo
```ssh
ssh-keygen -t ed25519 -f cloudgeeks -C "default"
```

#### yq just like jq for json, yq is for yaml
- https://dev.to/vikcodes/yq-a-command-line-tool-that-will-help-you-handle-your-yaml-resources-better-8j9
- https://mikefarah.gitbook.io/yq/v/v3.x/
- https://mikefarah.gitbook.io/yq/upgrading-from-v3
```yq
add-apt-repository ppa:rmescandon/yq
apt update -y
apt install yq -y
```

##### Docker Slim Installation
```dockerslim
curl -sL https://raw.githubusercontent.com/docker-slim/docker-slim/master/scripts/install-dockerslim.sh | sudo -E bash -
```
##### Kind kubernetes Cluster Setup
- https://github.com/quickbooks2018/aws/blob/master/kind-configuration-file

```kind
#!/bin/bash
# Purpose: Kubernetes Cluster


######################################
# Docker & Docker Compose Installation
######################################
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
rm -f get-docker.sh


#######################
# Kubectl Installation
#######################
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
kubectl version --client

####################
# Helm Installation
####################
# https://helm.sh/docs/intro/install/

curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
cp /usr/local/bin/helm /usr/bin/helm
rm -f get_helm.sh
helm version

###################
# Kind Installation
###################
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.17.0/kind-linux-amd64
# curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.0/kind-linux-amd64 

# Latest Version
# https://github.com/kubernetes-sigs/kind
# curl -Lo ./kind "https://kind.sigs.k8s.io/dl/v0.9.0/kind-$(uname)-amd64"
# curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.9.0/kind-linux-amd64
# curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.8.1/kind-linux-amd64
chmod +x ./kind

mv ./kind /usr/local/bin

#############
# Kind Config
#############
#cat << EOF > kind-config.yaml
#kind: Cluster
#apiVersion: kind.x-k8s.io/v1alpha4
#networking:
#  apiServerAddress: 0.0.0.0
#  apiServerPort: 8443
#EOF
  
#kind create --name cloudgeeks cluster --config kind-config.yaml --image kindest/node:v1.21.10
  
  # ssh -N -L 8443:0.0.0.0:8443 cloud_user@d8d0041c.mylabserver.com
  
  export KUBECONFIG=".kube/config"
  
 ####################  
 # Multi-Node Cluster
 ####################
 cat > kind-config.yaml <<EOF
# three node (two workers) cluster config
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
networking:
  apiServerAddress: 0.0.0.0
  apiServerPort: 8443
nodes:
- role: control-plane
- role: worker
- role: worker
EOF
  
 kind create --name cloudgeeks cluster --config kind-config.yaml --image kindest/node:v1.23.13
 
  #End
  ```

 - Helm cheat sheet
 
 - https://github.com/quickbooks2018/aws/blob/master/helm-useful-commands 

 ```helm
 https://artifacthub.io/

helm ls -A
helm repo ls
helm template dirname

####################################
# Helm Lint for Helm Syntax Checking
####################################
helm lint dirname

helm plugin ls

######################
# Basic Sample Release
######################
helm create hello
cd hello
helm install hello ./ -f values.yaml --namespace datree --create-namespace --debug --dry-run

cd ..
helm template -f hello/values.yaml --namespace datree --create-namespace hello/ --dry-run


helm install hello ./ -f values.yaml --namespace datree --create-namespace --debug


helm upgrade --install hello ./ -f values.yaml --namespace datree --create-namespace --debug
###############
# Helm Template
###############
-----------------------------------------
Note: Difference between Teample & Dryrun
-----------------------------------------
Template output is pure yaml does not need k8 connectivity with API Server
Dy Run out is not pure yaml does need k8 connectivity with k8 API Server

helm template dirname

helm template -f env.yaml helloworld 

 helm template -f env.yaml ./
 
 helm template -f helloworld/env.yaml helloworld/
 
helm upgrade --install nginx-alpine -f custom-values.yaml -f env.yaml ./ --namespace default --create-namespace

helm template -f hello/values.yaml --namespace datree --create-namespace hello/

helm template -f hello/values.yaml --namespace datree --create-namespace hello/ > pureyaml.yaml

helm template -f hello/values.yaml --namespace datree --create-namespace hello/ --dry-run # Note: dry run will talk to kubernetes API server & output of this is proper yaml


#############
# Diff Plugin
#############
# https://github.com/databus23/helm-diff

helm plugin ls

helm plugin install https://github.com/databus23/helm-diff 

helm history release-name -n learning

helm diff revision release-name 1 2

helm diff revision hello 1 2 -n learning

hub wordpress  --max-col-width=0  | grep -i bitnami/wordpress
helm repo add bitnami https://charts.bitnami.com/bitnami

########
# README
########
helm show readme bitnami/wordpress --version 10.0.3

##########
# Versions
##########
helm search repo wordpress --versions

########
# Values
########
helm show values bitnami/wordpress --version 10.0.3 

wordpressUsername: cloudgeeks
wordpressPassword: cloudgeeks
wordpressEmail: info@cloudgeeks.ca
wordpressFirstName: Muhammad
wordpressLastName: Asim
wordpressBlogName: cloudgeeks.ca
service: 
  type: LoadBalancer

##############
# Installation
##############
helm upgrade --install wordpress bitnami/wordpress  --values=wordpress-values.yaml --namespace wordpress --create-namespace --version 10.0.3 --debug

#######
# Watch
#######
watch -x kubectl get all --namespace wordpress

########
# Dryrun
########
helm install --dry-run --debug
 
 helm install nginx-alpine ./ -f values.yaml --namespace learning --create-namespace --debug --dry-run
 
             # release-name
helm install nginx-alpine --debug --dry-run helloworld

             # environment variable file
helm install nginx-alpine -f helloworld/env.yaml helloworld  --debug --dry-run

               # release-name-absent
helm install -f helloworld/env.yaml helloworld  --debug --dry-run 

helm install -f helloworld/env.yaml helloworld  --debug --dry-run --generate-name

###########
# Rollback
###########
             # release-name
helm rollback nginx-alpine 1 --debug

#########
# History
#########
helm history release-name --debug

##############
# Helm Secrets
##############
SOPS is an editor of encrypted files that supports YAML, JSON, ENV, INI and BINARY formats and encrypts with AWS KMS, GCP KMS, Azure Key Vault, age, and PGP
https://github.com/mozilla/sops/releases

curl -LO -# https://github.com/mozilla/sops/releases/download/v3.7.2/sops-v3.7.2.linux.amd64

mv sops-v3.7.2.linux.amd64 sops
chmod +x sops
mv sops /usr/local/bin

# Modern encryption tool age (AGE)
https://github.com/FiloSottile/age
age is a simple, modern and secure file encryption tool, format, and Go library.

apt install -y age

age-keygen -o key.txt

mkdir -p ~/.config/sops/age

mv key.txt ~/.config/sops/age/keys.txt

# this will create default encryption rules  # copy public key from key.txt age1z03hnu6s5a37hahytvl8y7zme669r47p0yz2zwfss5quz3t83sgs23n5ha
cat << EOF > .sops.yaml
creation_rules:
  -  age: age1zq6mygn07kzvt2ar435ea2jvy9pg52ta5zczjeq0kmjzw8cv0vcqr3hl0t

EOF

cat << EOF > values.secrets.yaml
username: "root"
password: "12345678"
EOF

#########
# Encrypt
##########
helm secrets enc values.secrets.yaml

#########
# Decrypt
#########
helm secrets dec values.secrets.yaml

helm secrets view values.secrets.yaml

export GPG_TTY=$(tty) # incase of error

################
# Helm NOTES.txt
################
cat << EOF > NOTES.txt
deployment command
helm upgrade --install nginx-alpine ./ --namespace learning --create-namespace
helm release name {{ .Release.Name }}
helm chart name {{ .Chart.Name }}
EOF

#################
# Sample Release
#################
helm create hello

helm ---> templates ---> secrets.yaml

cat << EOF > secrets.yaml
apiVersion: v1
kind: Secret
metadata:
  name: credentials
  labels:
    app: app
    chart: '{{ .Chart.Name }}-{{ .Chart.Version }}'
    release: '{{ .Release.Name }}'
    heritage: '{{ .Release.Service }}'
type: Opaque
data:
  password: {{ .Values.password | b64enc | quote }}
  username: {{ .Values.username | b64enc | quote }}

EOF

# Reference in deployment
cat << EOF > deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "hello.fullname" . }}
  labels:
    {{- include "hello.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "hello.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "hello.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "hello.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: MY_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: credentials
                  key: password
            - name: MY_USERNAME
              valueFrom:
                secretKeyRef:
                  name: credentials
                  key: username
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
EOF

######################################
# Helm command for secrets deployment
######################################
helm secrets install nginx-alpine ./ -f values.yaml -f values.secrets.yaml --namespace learning --create-namespace --debug

helm secrets upgrade --install nginx-alpine ./ -f values.yaml -f values.secrets.yaml --namespace learning --create-namespace --debug


##########
# Helm Get 
###########
helm get INSTALLATIONNAME

helm get INSTALLATIONNAME values
# this will show the values of last deployment via helm

helm get INSTALLATIONNAME values --all
# this will show all the values of last deployment via helm

helm get INSTALLATIONNAME values --revision 1
# this will show the values of specific deployment via helm

helm get INSTALLATIONNAME values --revision 2
# this will show the values of specific deployment via helm

###############
# Helm Manifest
################
helm get manifest INSTALLATIONNAME
# this will get the manifest of the existing last deployed 


helm get manifest INSTALLATIONNAME --revision 1
# this will get the manifest of the specific version deployed 


helm get manifest INSTALLATIONNAME --revision 2
# this will get the manifest of the specific version deployed 

##############
# Helm History
##############
kubectl get secrets -n default
# this will show also the helm versions as well

helm history INSTALLATIONNAME
# this will show all the deployed history with revisions

###############
# Helm RollBack
###############
helm rollback INSTALLATIONNAME 1
# Rollback to revision 1

helm rollback INSTALLATION 3
# Rollback to revision 3

##################################
# Helm Uninstall with Keep History
##################################
helm uninstall INSTALLATIONNAME --keep-history
# Even after you uninstall the history will be there in secrets section & you can again redeploy with Helm RollBack

##############
# Helm TimeOut 
##############
helm upgrade --install nginx-alpine -f custom-values.yaml -f env.yaml ./ --namespace default --create-namespace --wait --timeout 10m25s --debug

# this is usefull when your installation is taking more time example: image pull take time or network latency issues

#############
# Helm Atomic
#############
helm upgrade --install nginx-alpine -f custom-values.yaml -f env.yaml ./ --namespace default --create-namespace --atomic -timeout 10m25s
# atomic mean it will wait till all the k8 resources are succesfully deployed. default is 5 min
# default timeout is 5 minutes
# Note --wait be enabled automatically with ATOMIC
# this is very usefull in CICD, incase of failed existing deployment, it will automatically rollback to previously successfull deployment

####################################
# Helm Atomic With CleanUp on Failure
#####################################
helm upgrade --install nginx-alpine -f custom-values.yaml -f env.yaml ./ --namespace default --create-namespace --atomic -timeout 10m25s --cleanup-on-fail --debug
# atomic mean it will wait till all the k8 resources are succesfully deployed. default is 5 min
# cleanup-on-fail means it will cleanup all the resources if deployment is failed with new release
# default timeout is 5 minutes
# Note --wait be enabled automatically with ATOMIC
# this is very usefull in CICD, incase of failed existing deployment, it will automatically rollback to previously successfull deployment & with cleanup


############
# Helm Force
############
helm upgrade --install nginx-alpine -f custom-values.yaml -f env.yaml ./ --namespace default --create-namespace --force --debug
# Do not use this in CICD, sometimes you want to delete the deployment & recreate it, this option will be helpfull.

####################
# Packing helm chart
####################
helm package -d ${PWD}/helm ${PWD}/helm/cloudgeeks

########
# Issues 
########
# Helm Upgrade release stuck in pending state
# https://community.ops.io/the_cozma/k8s-fix-helm-release-failing-with-an-upgrade-still-in-progress-4660

[K8s] Fix Helm release failing with an upgrade still in progress
#
kubernetes
#
devops
#
tutorials
#
helm
This article applies to: Helm v3.8.0

Helm helps you manage Kubernetes applications — Helm Charts help you define, install, and upgrade even the most complex Kubernetes application. More details on Helm and the commands can be found in the official documentation.

Assuming you use Helm to handle your releases, you might end up in a case where the release will be stuck in a pending state and all subsequent releases will keep failing.

This could happen if:

you run the upgrade command from the cli and accidentally (or not) interrupt it or
you have two deploys running at the same time (maybe in Github Actions, for example)
Basically any interruption that occurred during your install/upgrade process could lead you to a state where you cannot install another release anymore.

In the release logs the failing upgrade will show an error similar to the following:
Error: UPGRADE FAILED: release <name> failed, and has been rolled back due to atomic being set: timed out waiting for the condition
Error: Error: The process '/usr/bin/helm3' failed with exit code 1
Error: The process '/usr/bin/helm3' failed with exit code 1
And the status will be stuck in: PENDING_INSTALL or PENDING_UPGRADE depending on the command you were running.

Because of this pending state when you run the command to list all release it will return empty:
> helm list --all                                                                               
NAME    NAMESPACE   REVISION    UPDATED STATUS  CHART   APP VERSION
So what can we do now? In this article we will look over the two options described below. Keep in mind that based on your setup it could be another issue, but I'm hoping that these two pointers will give you a place to start.

Roll back to the previous working version using the helm rollback command.
Delete the helm secret associated with the release and re-run the upgrade command.
So let's look at each option in detail.

First option: Roll back to the previous working version using the helm rollback command
From the official documentation:

This command rolls back a release to a previous revision.
The first argument of the rollback command is the name of a release, and the second is a revision (version) number. If this argument is omitted, it will roll back to the previous release.

So, in this case, let's get the history of the releases:
helm history <releasename> -n <namespace>
In the output you will notice the STATUS of your release with: pending-upgrade:
REVISION UPDATED                  STATUS          CHART     APP VERSION DESCRIPTION
1        Wed May 25 11:45:40 2022 DEPLOYED        api-0.1.0 1.16.0      Upgrade complete
2        Mon May 30 14:32:46 2022 PENDING_UPGRADE api-0.1.0 1.16.0      Preparing upgrade
Now let's perform the rollback by running the following command:
helm rollback <release> <revision> --namespace <namespace>
So in our case we run:
> helm rollback api 1 --namespace api
Rollback was a success.
After we get confirmation that the rollback was successful, we run the command to get the history again.

We now see we have two releases and that our rollback was successful having the STATUS is: deployed
> helm history api -n api

REVISION UPDATED                  STATUS      CHART     APP VERSION DESCRIPTION
1        Wed May 25 11:45:40 2022 SUPERSEEDED api-0.1.0 1.16.0      Upgrade complete
2        Mon May 30 14:32:46 2022 SUPERSEEDED api-0.1.0 1.16.0      Preparing upgrade
3        Mon May 30 14:45:46 2022 DEPLOYED    api-0.1.0 1.16.0      Rollback to 1
So what if the solution above didn't work?

Second option: Delete the helm secret associated with the release and re-run the upgrade command
First we get all the secrets for the namespace by running:
kubectl get secrets -n <namespace>
In the output you will notice a list of secrets in the following format:
NAME                        TYPE               DATA AGE
api                         Opaque             21   473d
sh.helm.release.v1.api.v648 helm.sh/release.v1 1    6d5h
sh.helm.release.v1.api.v649 helm.sh/release.v1 1    5d1h
sh.helm.release.v1.api.v650 helm.sh/release.v1 1    57m
So what's in a secret?

Helm3 makes use of the Kubernetes Secrets object to store any information regarding a release. These secrets are basically used by Helm to store and read it's state every time we run: helm list, helm history or helm upgrade in our case.

The naming of the secrets is unique per namespace.
The format follows the following convention:
sh.helm.release.v1.<release_name>.<release_version>.

There is a max of 10 secrets that are stored by default, but you can modify this by setting the --history-max flag in your helm upgrade command.

--history-max int limit the maximum number of revisions saved per release. Use 0 for no limit (default 10)

Now that we know what these secrets are used for, let's delete the helm secret associated with the pending release by running the following command:
kubectl delete secret sh.helm.release.v1.<release_name>.v<release_version> -n <namespace>
Finally, we re-run the helm upgrade command (either from command line or from your deployment workflow), which, if all was good so far, should succeed.

There is an open issue with Helm so hopefully these workaround won't be needed anymore. But it's open since 2018.

Of course there could be other cases or issues, but I hope this is a nice place to start. If you ran into something similar I would love to read your input in what the issue was and how you solved it especially since I didn't find the error message to be intuitive.


# Bitnami Old HELM Repos
# https://github.com/bitnami/charts/issues/10542
# repository: https://raw.githubusercontent.com/bitnami/charts/pre-2022/bitnami
```
  